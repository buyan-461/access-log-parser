#!/usr/bin/env bash

filePath='data.csv'
renderArray=''

while getopts "f:r" option; do
  case $option in
    f) filePath="$OPTARG"
    ;;
    r) renderArray='render'
    ;;
  esac
done

php index.php $filePath $renderArray