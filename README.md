Парсер для access логов nginx. Csv экспортируется из kibana. Группирует и оттдает csv + pie chart

required php7.4

`composer install`

`./run.sh -f <filePath.csv> -r`

Flags

-f - filepath (by default `data.csv`)

-r - output array in console (by default `false`)
