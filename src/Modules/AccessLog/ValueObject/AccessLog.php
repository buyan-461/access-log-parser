<?php

namespace Modules\AccessLog\ValueObject;

use Core\File\Service\CsvExporter;

class AccessLog
{
    private array $GET = [];
    private array $POST = [];
    private array $DELETE = [];
    private array $PUT = [];
    private int $countAll = 0;

    private const METHODS = ['GET', 'POST', 'PUT', 'DELETE'];
    private const REGEXP = '/(GET |POST |PUT |DELETE )(\/v1\/[\w+\-]+\/[\w+|\-]+)([\/|\?]?)/';
    private const DYNAMIC_URL = '/\/v1\/(record|list-filter)\/(.+)/';

    public function __construct(array $data)
    {
        $this->load($data);
    }

    public function render(): void
    {
        print_r('<pre>');
        print_r($this);
        die;
    }

    public function export(): void
    {
        $csvExporter = new CsvExporter();
        $csvExporter->addRow([
            'Method',
            'Url',
            'Percent',
            'Count',
        ]);
        foreach (self::METHODS as $method) {
            foreach ($this->$method as $url => $count) {
                $csvExporter->addRow([$method, $url, $this->percent($count), $count]);
            }
        }
        $csvExporter->export();
    }

    private function load(array $data): void
    {
        foreach ($data as $row) {
            if (isset($row[0])) {
                preg_match(self::REGEXP, $row[0], $matches);
                if (isset($matches[2])) {
                    $this->countAll++;
                    $method = trim($matches[1]);
                    $url = $this->url($matches[2]);
                    if (isset($this->$method[$url])) {
                        $this->$method[$url]++;
                    } else {
                        $this->$method[$url] = 1;
                    }
                }
            }
        }
        $this->sort();
    }

    private function url(string $url): string
    {
        $url = trim($url);
        preg_match(self::DYNAMIC_URL, $url, $matches);
        if (isset($matches[2])) {
            return str_replace($matches[2], '<tableName>', $url);
        }
        return $url;
    }

    private function sort(): void
    {
        foreach (self::METHODS as $method) {
            asort($this->$method);
        }
    }

    private function percent(int $count): float
    {
        return round(($count / $this->countAll) * 100, 1);
    }
}