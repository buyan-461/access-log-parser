<?php

namespace Core\File\Contract;

interface UploadedFileInterface
{
    /**
     * @return string
     */
    public function getFileName(): string;

    /**
     * @return string
     */
    public function getFilePath(): string;

    /**
     * @return string
     */
    public function getMimeType(): string;
}