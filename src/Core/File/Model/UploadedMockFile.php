<?php

namespace Core\File\Model;

use Core\File\Contract\UploadedFileInterface;

class UploadedMockFile implements UploadedFileInterface
{
    private string $fileName;
    private string $filePath;
    private string $mimeType;

    public function __construct(string $filePath)
    {
        $fileInfo = pathinfo($filePath);
        if (!file_exists($fileInfo['dirname'] . "/" . $fileInfo['basename'])) {
            throw new \Exception("File $filePath not exists.");
        }
        $this->fileName = $fileInfo['basename'];
        $this->filePath = $fileInfo['dirname'] . "/" . $fileInfo['basename'];
        $this->mimeType = $fileInfo['extension'];
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function getMimeType(): string
    {
        return $this->mimeType;
    }
}