<?php

namespace Core\File\Service;

class CsvExporter
{
    private $handle;
    private string $delimiter;

    public function __construct(string $delimiter = ',')
    {
        $this->delimiter = $delimiter;
        ob_start();
        $this->handle = fopen('tmp/log' . time() . '.csv', 'w');
        fwrite($this->handle, "\xEF\xBB\xBF");
    }

    public function export()
    {
        fclose($this->handle);
        echo ob_get_clean();
        exit();
    }

    public function addRow(array $row)
    {
        fputcsv($this->handle, $row, $this->delimiter);
    }
}