<?php

namespace Core\File\Service;

use Core\File\Contract\UploadedFileInterface;
use LimitIterator;
use SplFileObject;

class FileParser
{
    public function parse(UploadedFileInterface $file, int $offset = 0, int $limit = 500000): array
    {
        if (!$this->isFileSupported($file)) {
            throw new \Exception('File type not supported');
        }
        $delimiter = $this->getFileDelimiter($file->getFilePath());

        $csv = new SplFileObject($file->getFilePath());
        $csv->setFlags(SplFileObject::READ_CSV);
        $csv->setCsvControl($delimiter);

        if ($csv->eof()) {
            return [];
        }

        $data = [];
        $chunk = new LimitIterator($csv, $offset, $limit);
        foreach ($chunk as $line) {
            if (empty($line) || !isset($line[0])) {
                continue;
            }
            $data[] = $line;
        }
        return $data;
    }

    private function getFileDelimiter(string $file, int $checkLines = 100): string
    {
        $file = new SplFileObject($file);
        $delimiters = [',', ';'];
        $results = [];
        $i = 0;
        while ($file->valid() && $i <= $checkLines) {
            $line = $file->fgets();
            foreach ($delimiters as $delimiter) {
                $regExp = '/['.$delimiter.']/';
                $fields = preg_split($regExp, $line);
                if (count($fields) > 1) {
                    if (!empty($results[$delimiter])) {
                        $results[$delimiter]++;
                    } else {
                        $results[$delimiter] = 1;
                    }
                }
            }
            $i++;
        }
        $results = array_keys($results, max($results));
        return $results[0] ?? ';';
    }

    private function isFileSupported(UploadedFileInterface $file): bool
    {
        return $file->getMimeType() === 'csv';
    }
}
