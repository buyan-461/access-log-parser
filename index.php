<?php

require "vendor/autoload.php";

use Core\File\Model\UploadedMockFile;
use Core\File\Service\FileParser;
use Modules\AccessLog\ValueObject\AccessLog;

$file = new UploadedMockFile($argv[1]);

$parser = new FileParser;
$data = $parser->parse($file);

$log = new AccessLog($data);
$log->export();